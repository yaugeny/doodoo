var _ = require('underscore');

module.exports = {
    template: require('../../../templates/admin/grid.html'),
    name: 'grid',
    components: {
        pagination: require('./pagination.js')
    },
    props: {
        itemsPerPageOptions: {
            type: Array,
            default: function(){return [10, 25, 50]}
        },
        contentUrl: {
            type: String,
            required: true
        }
    },
    data: function() {return {
        searchQuery: '',
        list: [],
        columns: {},
        sortBy: '',
        sortOrder: 'ASC',
        itemsPerPage: this.itemsPerPageOptions[0],
        page: 1,
        pagesCount: 1,
        initialized: false,
        xhr: {}
    }},
    computed: {
        columnsSelected: function(){
            var that = this;
            return Object.keys(this.columns).filter(function(key){
                return that.columns[key].selected
            })
        }
    },
    methods: {
        refreshList: function() {
            var app = this;
            if (!_.isEmpty(app.xhr)) {
                app.xhr.abort();
            }
            app.xhr = $.ajax({
                dataType: "json",
                url: app.contentUrl,
                data: {
                    query: app.searchQuery,
                    per_page: app.itemsPerPage,
                    page: app.page,
                    sort_by: app.sortBy,
                    sort_order: app.sortOrder,
                    columns_selected: app.columnsSelected
                },
                beforeSend: function(){
                    $(app.$el).find('.a-table').addClass('a-table--loading');
                },
                complete: function(){
                    $(app.$el).find('.a-table').removeClass('a-table--loading');
                    app.loading = false;
                },
                success: function(data) {
                    app.list = data.result;
                    app.pagesCount = data.pagesCount;
                    app.sortBy = data.sortBy;
                    app.sortOrder = data.sortOrder;
                    app.columns = data.columns;
                    //app.searchQuery = data.query;
                    app.page = data.page;
                    if (!app.initialized) {
                        app.initialize();
                        app.initialized = true;
                    }
                }
            });
        },
        jumpToFirstPage: function() {
            this.page = 1
        },
        sortList: function(columnKey) {
            if (this.sortBy == columnKey) {
                this.toggleOrder();
            } else {
                this.sortBy = columnKey;
                this.sortOrder = 'ASC';
            };
        },
        toggleOrder: function() {
            this.sortOrder = (this.sortOrder == 'ASC' ? 'DESC' : 'ASC');
        },
        initialize: function() {
            var app = this;
            var varsRelatedToList = [
                'searchQuery',
                'itemsPerPage',
                'page',
                'sortBy',
                'sortOrder',
                'columnsSelected + ""'
            ];
            varsRelatedToList.forEach(function(variable) {
                app.$watch(variable, function(val, oldVal) {
                    this.refreshList();
                    return val;
                });
            });
        }
    },
    filters: {
        checkLastColumn: {
            read: function(val, key) {
                return val;
            },
            write: function(val, oldVal, key) {
                if (this.sortBy == key) {
                    return oldVal;
                } else {
                    return val;
                }
            }
        }
    },
    watch: {
        'pagesCount + sortBy + sortOrder': 'jumpToFirstPage',
        //'searchQuery + itemsPerPage + page + sortBy + sortOrder + columnsSelected': 'refreshList',
    },
    ready: function() {
        this.refreshList();
    }
};