import React from 'react'
import _ from 'underscore'
import $ from 'jquery'
import classNames from 'classnames'

import Table from './table'
import Pagination from './pagination'

export default class Grid extends React.Component {

    constructor() {
        super();

        this.state = {
            columns: [],
            selectedColumns: [],
            list: [],
            sort: {},
            searchQuery: '',
            currentPage: '',
            pagesCount: '',
        };

        this.initialized = false
    }

    render() {
        let {list,
             columns,
             selectedColumns,
             sort,
             searchQuery,
             currentPage,
             pagesCount} = this.state;

        let classes = {
            noResults: classNames({
                "a-grid__no-results": true,
                "a-grid__no-results--active": !this.state.pagesCount
            })
        };
        return (
            <div className="a-grid">
                <div className="a-grid__header">
                    <input className="g-input"
                           type="text"
                           value={searchQuery}
                           onChange={(e) => {this.setState({searchQuery: e.target.value})}}/>
                </div>
                <div className="a-grid__body">
                    <div className={classes.noResults}>Таблица пуста...</div>
                    <Table columns={columns}
                           selectedColumns = {selectedColumns}
                           sort={sort}
                           list={list}
                           parentSortReceiver={(sort)=>this.handleSortChange(sort)} />
                </div>
                <div className="a-grid__footer">
                    <Pagination parentPageReceiver={(page) => this.handlePageChange(page)}
                                currentPage={currentPage}
                                pagesCount={pagesCount}
                                pagesVisibilityRadius="5"/>
                </div>
                <div>{this.props.children}</div>
            </div>
        )
    }

    componentDidMount() {
        this.listRefresh();
    }

    componentDidUpdate(prevProps, prevState) {
        if (!this.initialized) {
            this.initialized = true
        } else {
            let pageChangeTriggers = ['searchQuery', 'sort'];
            if (this.paramsChanged(pageChangeTriggers, prevState, this.state)) {
                this.state.currentPage == 1 ?
                    this.listRefresh() :
                    this.jumpToFirstPage();
            }

            let listTriggers = ['currentPage'];
            if (this.paramsChanged(listTriggers, prevState, this.state)) {
                this.listRefresh();
            }
        }
    }

    listRefresh() {
        let {searchQuery: search_query,
             currentPage: current_page,
             sort} = this.state;
        let initialized = this.initialized;

        if (!_.isEmpty(this.serverRequest)) {
            this.serverRequest.abort();
        }

        this.serverRequest = $.ajax({
            url: this.props.source,
            data: {
                search_query,
                current_page,
                sort,
                uninitialized: !initialized?
                    true : undefined
            },
            success(data) {
                this.setState(data);
            },
            context: this
        });
    }

    handlePageChange(newPage) {
        let {pagesCount} = this.state;

        if (newPage === '') {
            this.setState({currentPage: newPage});
        } else if (!isNaN(newPage)) {
            if (newPage > pagesCount) this.setState({currentPage: pagesCount});
            else if (newPage < 1) this.setState({currentPage: 1});
            else this.setState({currentPage: parseInt(newPage)})
        }
    }

    handleSortChange(sort) {
        this.setState({sort})
    }

    paramsChanged(keys, prevObj, newObj) {
        let triggersOldStates = keys.map((key) => prevObj[key]);
        let triggersNewStates = keys.map((key) => newObj[key]);
        return !_.isEqual(triggersOldStates, triggersNewStates);
    }

    jumpToFirstPage() {
        this.setState({currentPage: 1});
    }
};