import React from 'react'
import classNames from 'classnames'
import _ from 'underscore'

export default class Pagination extends React.Component {

    render() {
        let {pagesCount, currentPage, parentPageReceiver} = this.props;
        let pagesSequence = this.pageSequence();
        let classes = {
            pagination: classNames({
                "a-pagination": true,
                "g-hidden": pagesCount == 0
            }),
            leftGap: classNames({
                "a-pagination__gap": true,
                "g-hidden": _.first(pagesSequence) == 1
            }),
            rightGap: classNames({
                "a-pagination__gap": true,
                "g-hidden": _.last(pagesSequence) == pagesCount
            }),
            forwardButtons: classNames({
                "a-pagination__control": true,
                "a-pagination__control--disabled": currentPage == pagesCount
            }),
            backButtons: classNames({
                "a-pagination__control": true,
                "a-pagination__control--disabled": currentPage <= 1
            }),
            pageLink: (pageNum) => classNames ({
                "a-pagination__link": true,
                "a-pagination__link--active":
                pageNum == this.props.currentPage ||
                pageNum == 1 && this.props.currentPage == 0
            })
        };
        return (
            <div className={classes.pagination}>
                <div className="a-pagination__primary">
                    <div className="a-pagination__left-control">
                        <button className={classes.backButtons}
                                onClick={(e) => parentPageReceiver(1)}>
                            <i className="g-fa g-fa-angle-double-left" />
                        </button>
                        <button className={classes.backButtons}
                                onClick={(e) => parentPageReceiver(currentPage - 1)}>
                            <i className="g-fa g-fa-angle-left" />
                        </button>
                    </div>
                    <div className="a-pagination__pages">
                        <span className={classes.leftGap}>...</span>
                            {pagesSequence.map((n, i) =>
                                <button key={i}
                                        className={classes.pageLink(n)}
                                        onClick={() => parentPageReceiver(n)}>
                                    {n}
                                </button>
                            )}
                        <span className={classes.rightGap}>...</span>
                    </div>
                    <div className="a-pagination__right-control">
                        <button className={classes.forwardButtons}
                                onClick={(e) => parentPageReceiver(currentPage + 1)}>
                            <i className="g-fa g-fa-angle-right" />
                        </button>
                        <button className={classes.forwardButtons}
                                onClick={(e) => parentPageReceiver(pagesCount)}>
                            <i className="g-fa g-fa-angle-double-right" />
                        </button>
                    </div>
                </div>
                <div className="a-pagination__secondary">
                    Страница
                    <input className="a-pagination__input g-input"
                           type="text"
                           value={this.renderedPageNumber()}
                           onChange={(e) => this.handlePageChange(e)}
                           onBlur={(e) => this.handlePageChange(e)}
                    />
                    из {pagesCount}
                </div>
            </div>
        )
    }

    renderedPageNumber() {
        let {currentPage} = this.props;
        if (currentPage == 0) return '';
        else return currentPage;
    }

    handlePageChange(e) {
        let {parentPageReceiver} = this.props,
            newPage = e.target.value;

        if (newPage == '' && e.type == "blur") {
            parentPageReceiver(1)
        } else {
            parentPageReceiver(newPage)
        }
    }
    
    pageSequence() {
        let {pagesCount, currentPage, pagesVisibilityRadius} = this.props,
            leftSideCount = 0,
            rightSideCount = 0,
            out = [];
        
        /*
         Считаем от текущей страницы, сколько должно быть видно слева страниц и справа.
         Если текущая страница ближе к началу, сначала считается кол-во слева, а потом
         кол-во справа с учетом оставшихся видимых страниц. Если текущая страница
         ближе к концу - наоборот, сначала считается кол-во справа, чтобы учесть
         кол-во оставшихся видимых страниц слева.
         */
        let p  = currentPage,
            t  = pagesCount,
            r  = pagesVisibilityRadius,
            ls = leftSideCount,
            rs = rightSideCount;
        
        if (p < t - p) {
            ls = (p > r) ? r : p;
            rs = (t - p > r*2 - ls) ? r*2 - ls : t - p;
        } else {
            rs = (t - p > r) ? r : t - p;
            ls = (p > r*2 - rs) ? r*2 - rs : p;
        }

        for (var i = p - ls; i < p + rs; i++) {
            out.push(i + 1);
        }

        return out;
    }
    
}
