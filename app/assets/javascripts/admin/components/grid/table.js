import React from 'react'
import classNames from 'classnames'
import _ from 'underscore'

export default class Table extends React.Component {

    render() {
        let {columns, list, sort, selectedColumns} = this.props;
        let classes = {
            table: classNames({
                "a-table": true,
                "g-hidden": _.isEmpty(list)
            }),
            th: (columnKey) => classNames({
                "a-table__cell": true,
                "a-table__cell--header": true,
                "a-table__cell--header--sortable": true,
                "a-table__cell--header--sortable--active": columnKey == sort.by,
                "a-table__cell--header--sortable--active--desc": columnKey == sort.by && sort.order == 'DESC'
            })
        };
        return (
            <table className={classes.table}>
                <thead>
                <tr>
                    {selectedColumns.map((columnKey, i) =>
                        <th key={i}
                            className={classes.th(columnKey)}
                            onClick={() => this.handleSortChange(columnKey)}>
                            {_.findWhere(columns, {key: columnKey}).name}
                        </th>
                    )}
                </tr>
                </thead>
                <tbody>
                {list.map((item, i) =>
                    <tr key={i} className="a-table__row">
                        {selectedColumns.map((columnKey, i) =>
                            <td key={i} className="a-table__cell">{item[columnKey]}</td>
                        )}
                    </tr>
                )}
                </tbody>
            </table>
        )
    }

    handleSortChange(column) {
        let {sort: oldSort, parentSortReceiver} = this.props;
        let newSort = _.clone(oldSort);
        if (oldSort.by != column) {
            newSort = {by: column, order: "ASC"};
        } else {
            newSort.order = oldSort.order == "ASC" ? "DESC" : "ASC";
        }
        parentSortReceiver(newSort)
    }
}