
module.exports = {
    name: 'pagination',
    template: require('../../../templates/admin/pagination.html'),
    props: {
        pagesVisibilityRadius: {
            type: Number,
            default: 5
        },
        pagesCount: {
            type: Number,
            required: true
        },
        page: {
            type: Number,
            required: true,
            twoWay: true
        }
    },
    data: function() {
        return {
            temp: 1
        }
    },
    computed: {
        controls: function() {
            var res = {
                f: this.page != this.pagesCount,
                b: this.page != 1,
                lg: this.pageSequence[0] != 1,
                rg: this.pageSequence[this.pageSequence.length-1] != this.pagesCount,
            };
            return res;
        },
        pageSequence: function() {
            var page = this.page,
                radius = this.pagesVisibilityRadius,
                pagesTotal = this.pagesCount,
                leftSideCount = 0,
                rightSideCount = 0,
                out = [];

            /*
             Считаем от текущей страницы, сколько должно быть видно слева страниц и справа.
             Если текущая страница ближе к началу, сначала считается кол-во слева, а потом
             кол-во справа с учетом оставшихся видимых страниц. Если текущая страница
             ближе к концу - наоборот, сначала считается кол-во справа, чтобы учесть
             кол-во оставшихся видимых страниц слева.
             */
            if (page < pagesTotal - page) {
                leftSideCount = (page > radius) ? radius : page;
                rightSideCount = (pagesTotal - page > radius*2 - leftSideCount) ? radius*2 - leftSideCount : pagesTotal - page;
            } else {
                rightSideCount = (pagesTotal - page > radius) ? radius : pagesTotal - page;
                leftSideCount = (page > radius*2 - rightSideCount) ? radius*2 - rightSideCount : page;
            }

            for (var i = page - leftSideCount; i < page + rightSideCount; i++) {
                out.push(i + 1);
            }

            return out;
        },
    },
    filters: {
        numberMinMaxValidation: require('../../misc/filters/numberMinMaxValidation.js')
    }
};