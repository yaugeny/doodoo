import './basic'

import React from 'react'
import ReactDOM from 'react-dom'

import Grid from './components/grid'

ReactDOM.render(<Grid source="/admin/users.json" />, document.getElementById('react'));