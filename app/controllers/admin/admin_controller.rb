class Admin::AdminController < ApplicationController
  before_action do
    authenticate_user!
    authorize :dashboard, :use?
  end
  layout 'admin/basic'
end