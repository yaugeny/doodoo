class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?

  # Intercept pundit not_autorized error
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  include ApplicationHelper
  include Pundit

  private

  # Intercept pundit not_autorized error
  def user_not_authorized(exception)
    render 'errors/unauthorized', status: :unauthorized, layout: 'error'
  end

  protected

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
    end

end
