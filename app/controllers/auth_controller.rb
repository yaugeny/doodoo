class AuthController < ApplicationController

  before_action :guest_only, except: [:logout]
  before_action :store_referer, only: [:login, :signup]

  def login
  end

  layout 'centered'

  def login_post
    @user = User.find_by(email: params[:data][:email].downcase)
    if @user && @user.authenticate(params[:data][:password])
      log_in @user
      redirect_back_or root_path
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'login'
    end
  end

  def signup
    @user = User.new
  end

  def signup_post
    @user = User.new(user_params)
    if @user.save
      UserMailer.signup_successful(@user, params['password']).deliver_now
      log_in @user
      flash[:success] = "Welcome!"
      redirect_back_or root_path
    else
      render 'signup'
    end
  end

  def logout
    log_out if logged_in?
    redirect_to request.referer ? :back : root_path
  end



  private

    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end

    def store_referer
      referer = request.referer unless logged_in?

      if !referer.nil? and request.host == URI(referer).host and not [login_url, signup_url].include? referer
        session[:forwarding_url] = referer
      end
    end
end
