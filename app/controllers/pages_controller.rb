class PagesController < ApplicationController
  before_action :logged_in_user, only: [:private]

  def home
  end

  def about
  end

end
