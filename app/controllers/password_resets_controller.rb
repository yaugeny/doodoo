class PasswordResetsController < ApplicationController
  before_action :get_user,   only: [:password_reset, :password_reset_post]
  before_action :valid_user, only: [:password_reset, :password_reset_post]
  before_action :check_expiration, only: [:password_reset, :password_reset_post]
  before_action :guest_only

  layout 'centered'

  def password_forgot
  end

  def password_forgot_post
    @user = User.find_by(email: params[:data][:email].downcase)
    if @user
      @user.create_reset_digest
      UserMailer.password_reset(@user).deliver_now
      flash[:info] = "Email sent with password reset instructions"
      redirect_to root_url
    else
      flash.now[:danger] = "Email address not found"
      render 'password_forgot'
    end
  end

  def password_reset
  end

  def password_reset_post
    if params[:user][:password].empty?
      @user.errors.add(:password, "can't be empty")
      render 'password_reset'
    elsif @user.update_attributes(user_params)
      log_in @user
      @user.clear_reset_digest
      flash[:success] = "Password has been reset."
      redirect_to root_path
    else
      render 'password_reset'
    end
  end

  private

    def user_params
      params.require(:user).permit(:password, :password_confirmation)
    end

    def get_user
      @user = User.find_by(email: params[:email])
    end

    # Confirms a valid user.
    def valid_user
      unless @user && @user.authenticated?(:reset, params[:reset_token])
        flash[:danger] = "Error!"
        redirect_to root_url
      end
    end

    # Checks expiration of reset token.
    def check_expiration
      if @user.password_reset_expired?
        flash[:danger] = "Password reset has expired."
        redirect_to new_password_reset_url
      end
    end
end
