class SessionsController < Devise::SessionsController

  def create
    # Make users always rememberable
    params[:user].merge!(remember_me: true)
    # byebug
    super
  end

end