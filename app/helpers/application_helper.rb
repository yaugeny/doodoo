module ApplicationHelper

  include AuthHelper
  include RolesHelper

  def log(message)
    logger.debug "\n #{message} \n".green.on_white
  end

end
