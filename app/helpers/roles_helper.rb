module RolesHelper

  def can?(permissions)
    if permissions.is_a? Array
      permissions.each do |permission|
        return true if current_user.has_permission? permission
      end
      return false
    else
      current_user.has_permission? permissions
    end
  end

end