class Permission < ActiveRecord::Base
  validates :name, presence: true,
            length: { maximum: 30 }

  has_and_belongs_to_many :roles, ->{ uniq }
end
