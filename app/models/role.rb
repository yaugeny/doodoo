class Role < ActiveRecord::Base
  validates :name, presence: true,
            length: { maximum: 30 }

  validates :level, numericality: { only_integer: true,
                                  greater_than_or_equal_to: 0,
                                  less_than_or_equal_to: 100 }

  has_and_belongs_to_many :permissions, ->{ uniq }
  has_and_belongs_to_many :users, ->{ uniq }
end
