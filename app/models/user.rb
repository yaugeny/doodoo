class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :name, presence: true,
            length: { maximum: 50 }

  has_and_belongs_to_many :roles, -> { uniq }

  def assign_roles(given_roles)
    roles << Role.where(name: given_roles)
  end

  def has_roles?(given_roles)
    !!roles.where(name: given_roles)
  end

  def is_admin?
    has_roles? :admin
  end

  def has_permission?(given)
    permission = Permission.find_by(name: given) or return false
    permission.roles.exists? self.roles
  end

  def level
    levels = []
    roles.each {|role| levels << role.level}
    levels.min || 100
  end

  def higher_rank?(user)
    level < user.level
  end

  # Devise patch for remembering users by default
  def remember_me
    true
  end

=begin
  attr_accessor :remember_token, :reset_token
  before_save :email_downcase

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, presence: true,
            length: { maximum: 255 },
            format: { with: VALID_EMAIL_REGEX },
            uniqueness: { case_sensitive: false }

  validates :name, presence: true,
            length: { maximum: 50 }

  has_secure_password
  validates :password, presence: true,
            length: { minimum: 6 },
            allow_nil: true

  has_and_belongs_to_many :roles, -> { uniq }

  # Remembers a user in the database for use in persistent sessions.
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  # Returns true if the given token matches the digest.
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  # Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end

  # Sets the password reset attributes.
  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest,  User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  def clear_reset_digest
    self.reset_token = nil
    update_attribute(:reset_digest,  nil)
    update_attribute(:reset_sent_at, nil)
  end

  # Returns true if a password reset has expired.
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end

  def assign_roles(given_roles)
    roles << Role.where(name: given_roles)
  end

  def has_roles?(given_roles)
    !!roles.where(name: given_roles)
  end

  def is_admin?
    has_roles? :admin
  end

  def has_permission?(given)
    permission = Permission.find_by(name: given) or return false
    permission.roles.exists? self.roles
  end

  def level
    levels = []
    roles.each {|role| levels << role.level}
    levels.min || 100
  end

  def higher_than?(user)
    level < user.level
  end

  class << self

    # Returns the hash digest of the given string.
    def digest(string)
      cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
          BCrypt::Engine.cost
      BCrypt::Password.create(string, cost: cost)
    end

    # Returns a random token.
    def new_token
      SecureRandom.urlsafe_base64
    end

  end



  private

    # Converts email to all lower-case.
    def email_downcase
      email.downcase!
    end
=end

end
