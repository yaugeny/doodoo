class DashboardPolicy < Struct.new(:user, :dashboard)
  def use?
    user.has_permission? 'use_panel'
  end
end