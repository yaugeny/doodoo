class UserPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def edit?
    ( @user.has_permission? 'update_users' and @user.higher_rank? record ) or @user.email == record.email
  end

  def destroy?
    ( @user.has_permission? 'delete_users' and @user.higher_rank? record ) or @user.email == record.email
  end

end
