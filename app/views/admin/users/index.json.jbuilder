json.pagesCount @users.total_pages
json.list @users do |user|
  @columns_available.each do |attr|
    json.extract! user, attr
  end
end
if @uninitialized
  json.currentPage @users.current_page
    json.columns @columns_available do |attr|
      json.key attr
      json.name User.human_attribute_name attr
    end
  json.selectedColumns @selected_columns
  json.sort @sort
end