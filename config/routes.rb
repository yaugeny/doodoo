Rails.application.routes.draw do

  root 'pages#home'

  devise_for :users, :controllers => {:sessions => 'sessions'}

  get 'about',   to: 'pages#about'

  # Errors
  match '/401', to: 'errors#unauthorized',          via: :all
  match '/403', to: 'errors#forbidden',             via: :all
  match '/404', to: 'errors#not_found',             via: :all
  match '/422', to: 'errors#unprocessable_entity',  via: :all
  match '/500', to: 'errors#internal_server_error', via: :all

  # Admin
  namespace :admin do
    root 'pages#home'
    resources :users
  end

end
