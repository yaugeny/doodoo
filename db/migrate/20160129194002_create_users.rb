class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      #t.string :email
      t.string :name
      #t.string :password_digest
      #t.string :remember_digest
      #t.string :reset_digest
      #t.datetime :reset_sent_at

      t.timestamps null: false
    end
    #add_index :users, :email, unique: true
  end
end
