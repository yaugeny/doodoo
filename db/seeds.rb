user = User.new(name:  "A A",
                email: "a@a.a",
                password:              "password",
                password_confirmation: "password")

user.skip_confirmation!
user.save!

user = User.new(name:  "B B",
                email: "b@b.b",
                password:              "password",
                password_confirmation: "password")

user.skip_confirmation!
user.save!

user = User.new(name:  "C C",
                email: "c@c.c",
                password:              "password",
                password_confirmation: "password")

user.skip_confirmation!
user.save!

Permission.create!(name: 'use_panel')
Permission.create!(name: 'create_users')
Permission.create!(name: 'read_users')
Permission.create!(name: 'update_users')
Permission.create!(name: 'delete_users')

Role.create(name: 'admin', level: 0).permissions    << Permission.all
Role.create(name: 'manager', level: 50).permissions << Permission.where(name: ['use_panel', 'read_users'])

User.first.assign_roles  :admin
User.second.assign_roles :manager

if ['development', 'staging'].include? Rails.env
  100.times do
    user = User.new(name:  Faker::Name.name,
                    email: Faker::Internet.email,
                    password:              "password",
                    password_confirmation: "password")
    user.skip_confirmation!
    user.save!
  end
end