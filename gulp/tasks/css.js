var config = require('../config');
if(!config.tasks.css) return;

var gulp       = require('gulp'),
    sass       = require('gulp-sass'),
    minifyCSS  = require('gulp-minify-css'),
    rename     = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps'),
    notify     = require('gulp-notify'),
    check      = require('gulp-if');

var paths = config.tasks.css;

module.exports = function () {
    return gulp.src(paths.src)
        //.pipe(check(!args.production, sourcemaps.init()))
        .pipe(sass().on('error', notify.onError("Error: <%= error.message %>")))
        .pipe(gulp.dest(paths.dest))
        .pipe(minifyCSS())
        .pipe(rename({suffix: '.min'}))
        //.pipe(check(!args.production, sourcemaps.write('../maps')))
        .pipe(gulp.dest(paths.dest))
};