var config = require('../config');
if(!config.tasks.fonts) return;

var gulp       = require('gulp');

var paths = config.tasks.fonts;

module.exports = function() {
    return gulp.src(paths.src)
        .pipe(gulp.dest(paths.dest));
};