var config = require('../config');
if(!config.tasks.images) return;

var gulp       = require('gulp'),
    jpegoptim  = require('imagemin-jpegoptim'),
    pngquant   = require('imagemin-pngquant'),
    optipng    = require('imagemin-optipng'),
    svgo       = require('imagemin-svgo');

var paths = config.tasks.images;

module.exports = function () {
    return gulp.src(paths.src)
        .pipe(pngquant({quality: '100', speed: 4})())
        .pipe(optipng({optimizationLevel: 3})())
        .pipe(jpegoptim({max: 100})())
        //.pipe(svgo()())
        .pipe(gulp.dest(paths.dest));
};