var config = require('../config');
if(!config.tasks.js) return;

var gulp       = require('gulp'),
    rename     = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps'),
    check      = require('gulp-if'),
    glob       = require('glob'),
    stringify  = require('stringify'),
    browserify = require('browserify'),
    babelify   = require('babelify'),
    source     = require('vinyl-source-stream'),
    es         = require('event-stream'),
    notify     = require('gulp-notify'),
    path       = require('path'),
    webpack    = require('webpack-stream');

var paths = config.tasks.js;

module.exports = function(done) {

    /*return gulp.src("./app/assets/javascripts/admin/!*.js")
        .pipe(webpack({
            module: {
                loaders: [
                    {
                        test: /\.jsx?$/,
                        exclude: /(node_modules|bower_components)/,
                        loader: 'babel', // 'babel-loader' is also a legal name to reference
                        query: {
                            presets: ['es2015', 'react']
                        }
                    },
                ],
            },
        }))
        .pipe(gulp.dest(paths.dest));*/

    paths.src.forEach(function(pattern){
        glob(pattern, function(err, files) {

            if(err) done(err);

            var tasks = files.map(function(entry) {

                var fileWithDir = (
                    path.parse(path.dirname(entry)).name +
                    '/' +
                    path.parse(entry).base
                );

                return browserify({ entries: [entry] })
                    .transform(stringify, {
                        appliesTo: { includeExtensions: ['.html'] },
                        //minify: true
                    })
                    .transform(babelify, {presets: ["es2015", "react"]})
                    .bundle()
                    .on('error', notify.onError("Error: <%= error.message %>"))
                    .pipe(source(fileWithDir))
                    .pipe(gulp.dest(paths.dest))

            });

            es.merge(tasks).on('end', done);

        });
    });

};