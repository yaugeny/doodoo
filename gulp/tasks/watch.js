var config = require('../config');
if(!config.tasks.watch) return;

var gulp       = require('gulp'),
    watch      = require('gulp-watch');

var paths = config.tasks.watch;

module.exports = function() {
    Object.keys(paths).forEach(function(key) {
        gulp.src(paths[key])
            .pipe(watch(paths[key], function() {
                gulp.start(key);
            }));
    });

};