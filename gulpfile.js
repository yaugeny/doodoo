var gulp = require('./gulp')([
    'css',
    'images',
    'js',
    'watch',
    'fonts'
]);

gulp.task('default', ['css', 'js', 'images', 'fonts']);