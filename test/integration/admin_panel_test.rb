require 'test_helper'

class AdminPanelTest < ActionDispatch::IntegrationTest

  setup do
    @admin = Role.where(name: 'admin').first.users.first
    @user = User.includes(:roles).where(roles: {id: nil}).first
  end

  test 'admin panel is unavailable for a simple user' do
    login_as @user
    assert page.has_no_link? '', href: admin_root_path
    visit admin_root_path
    assert page.status_code == 401
  end

  test 'admin panel is unavailable for a guest and has correct layout' do
    visit admin_root_path
    assert page.has_no_link? '', href: destroy_user_session_path
  end

  test 'admin panel is available for an admin' do
    login_as @admin
    assert page.has_link? '', href: admin_root_path
    visit admin_root_path
    assert page.status_code == 200
  end

end
