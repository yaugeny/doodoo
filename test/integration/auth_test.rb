require 'test_helper'

class AuthTest < ActionDispatch::IntegrationTest



  # Login

  test 'get login page' do
    visit new_user_session_path
    see_login_form
  end

  test 'unsuccessful login' do
    visit new_user_session_path
    submit_form
    assert page.has_current_path? new_user_session_path
  end

=begin
  test 'successful login and redirect' do
    visit about_path
    first("[href='#{new_user_session_path}']")
    assert page.has_current_path? login_path
    fill_in 'data[email]',    with: @user.email
    fill_in 'data[password]', with: 'password'
    submit_form
    assert page.has_current_path? about_path
  end
=end

  test 'forwarding to private page' do
    visit admin_root_path
    see_login_form
    fill_in 'user[email]',    with: @user.email
    fill_in 'user[password]', with: 'password'
    submit_form
    assert page.has_current_path? admin_root_path
  end



  # Signup

  test 'get signup page' do
    visit new_user_registration_path
    see_signup_form
  end

  test 'unsuccessful signup' do
    visit new_user_registration_path
    submit_form
    see_signup_form
  end

  test 'unsuccessful signup with empty password' do
    visit new_user_registration_path
    empty_pass = '      '
    fill_in 'user[email]', with: 'email@email.com'
    fill_in 'user[name]', with: 'Guy'
    fill_in 'user[password]', with: empty_pass
    fill_in 'user[password_confirmation]', with: empty_pass
    submit_form
    see_signup_form
  end

  test 'unsuccessful signup with empty name' do
    visit new_user_registration_path
    fill_in 'user[email]', with: 'email@email.com'
    fill_in 'user[name]', with: ''
    fill_in 'user[password]', with: 'password'
    fill_in 'user[password_confirmation]', with: 'password'
    submit_form
    see_signup_form
  end

  test 'unsuccessful signup with invalid emails' do
    visit new_user_registration_path
    invalid_addresses = %w[foo@bar..com user@example,com user_at_foo.org user.name@example. foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      fill_in 'user[email]', with: invalid_address
      fill_in 'user[name]', with: 'Guy'
      fill_in 'user[password]', with: 'password'
      fill_in 'user[password_confirmation]', with: 'password'
      submit_form
      see_signup_form
    end
  end

  test 'successful signup and confirmation and resending conf. email' do
    count = User.all.count
    mails_count = emails.count

    visit new_user_registration_path
    fill_in 'user[email]', with: @new_user.email
    fill_in 'user[name]', with: @new_user.name
    fill_in 'user[password]', with: 'password'
    fill_in 'user[password_confirmation]', with: 'password'
    submit_form
    assert emails.count == mails_count + 1
    assert User.all.count == count + 1
    assert !User.find_by(email: @new_user.email).nil?

    visit user_confirmation_path
    fill_in 'user[email]', with: @new_user.email
    submit_form
    assert emails.count == mails_count + 2

    open_email @new_user.email
    current_email.first("a").click
    see_login_form
    fill_in 'user[email]', with: @new_user.email
    fill_in 'user[password]', with: 'password'
    submit_form
    assert page.has_current_path? root_path

    visit user_confirmation_path
    fill_in 'user[email]', with: @new_user.email
    submit_form
    assert emails.count == mails_count + 2
  end



  # Helpers

  def see_login_form
    assert page.has_css? 'input[name="user[email]"]'
    assert page.has_css? 'input[name="user[password]"]'
  end

  def see_signup_form
    assert page.has_css? 'input[name="user[email]"]'
    assert page.has_css? 'input[name="user[password]"]'
    assert page.has_css? 'input[name="user[password_confirmation]"]'
    assert page.has_css? 'input[name="user[name]"]'
  end

end
