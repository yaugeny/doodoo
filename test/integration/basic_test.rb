require 'test_helper'

class BasicTest < ActionDispatch::IntegrationTest
  test 'get home page' do
    login_as @user
    assert page.has_current_path? root_path
    assert page.has_content? @user.name
  end
end
