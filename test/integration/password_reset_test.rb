require 'test_helper'

class PasswordResetTest < ActionDispatch::IntegrationTest

  test 'get forgot password page' do
    visit new_user_session_path
    first("[href='#{new_user_password_path}']").click
    assert page.has_css? 'input[name="user[email]"]'
  end

  test 'signed in user cant get reset password page'  do
    login_as @user
    visit new_user_password_path
    assert page.has_current_path? root_path
  end

  test 'password reset process' do
    emails_count = emails.count

    visit new_user_password_path
    submit_form
    assert page.has_css? 'input[name="user[email]"]'
    fill_in 'user[email]', with: 'wrong@email.com'
    submit_form
    assert page.has_css? 'input[name="user[email]"]'
    fill_in 'user[email]', with: @user.email
    submit_form
    assert page.has_current_path? new_user_session_path
    # TODO: тест flash

    emails.count == emails_count + 1
    open_email @user.email
    current_email.first("a").click
    assert page.has_current_path? edit_user_password_path, only_path: true
    see_new_password_form
    submit_form
    see_new_password_form
    fill_in 'user[password]',              with: 'newpassword'
    fill_in 'user[password_confirmation]', with: 'newpasswordwrong'
    submit_form
    see_new_password_form
    fill_in 'user[password]',              with: 'newpassword'
    fill_in 'user[password_confirmation]', with: 'newpassword'
    submit_form
    assert page.has_current_path? root_path
    assert page.has_text? @user.name
    #delete_via_redirect destroy_user_session_path
    #save_and_open_page

=begin
    visit new_user_session_path
    fill_in 'user[email]', with: @user.email
    fill_in 'user[password]', with: 'password'
    submit_form
    assert page.has_current_path? new_user_session_path
    fill_in 'user[email]', with: @user.email
    fill_in 'user[password]', with: 'newpassword'
    submit_form
    assert page.has_current_path? root_path
    assert page.has_text? @user.name
=end
  end

  def see_new_password_form
    assert page.has_css? 'input[name="user[password]"]'
    assert page.has_css? 'input[name="user[password_confirmation]"]'
  end

end
