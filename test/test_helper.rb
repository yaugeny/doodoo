ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'capybara'
require 'capybara/dsl'
require 'capybara/email'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...

end



# Capybara configs

class ActionDispatch::IntegrationTest
  # Make the Capybara DSL available in all integration tests
  include Capybara::DSL
  include Capybara::Email::DSL

  # Warden helpers for Capybara
  include Warden::Test::Helpers
  Warden.test_mode!

  # Reset sessions and driver between tests
  # Use super wherever this method is redefined in your individual test classes
  def teardown
    Capybara.reset_sessions!
    Capybara.use_default_driver
    Warden.test_reset!
  end

end

Capybara.app = Rails.application



# Integration tests helpers

class ActionDispatch::IntegrationTest

  setup do
    @user = User.first
    @new_user = User.new name: 'Mr.New', email: 'mr.new@zigmund.ru'
  end

  def submit_form
    first('[type="submit"]').click
  end

  def login_as(user, options = {})
    password = options[:password] || 'password'
    visit new_user_session_path
    fill_in 'user[email]',    with: user.email
    fill_in 'user[password]', with: password
    find('[type="submit"]').click
  end

  def last_email
    ActionMailer::Base.deliveries.last
  end

  def emails
    ActionMailer::Base.deliveries
  end

end